import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent {

  loading: boolean;

  // paises: any[] = [];
  nuevasCanciones: any[] = [];

  constructor( private spotify: SpotifyService ) {

    this.loading = true;
    this.spotify.getNewReleases()
      .subscribe(( res:any ) =>{
        this.nuevasCanciones = res;
        this.loading = false;
      })
   }

}
